import { TestBed } from '@angular/core/testing';

import { AdministradorasService } from './administradoras.service';

describe('AdministradorasService', () => {
  let service: AdministradorasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdministradorasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
