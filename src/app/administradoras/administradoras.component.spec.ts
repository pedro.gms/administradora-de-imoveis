import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministradorasComponent } from './administradoras.component';

describe('AdministradorasComponent', () => {
  let component: AdministradorasComponent;
  let fixture: ComponentFixture<AdministradorasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministradorasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministradorasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
