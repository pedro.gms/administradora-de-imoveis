import { Component, OnInit } from '@angular/core';
import { AdministradorasService } from './administradoras.service';
import { Administradoras } from './administradoras.model';

@Component({
  selector: 'app-administradoras',
  templateUrl: './administradoras.component.html',
})
export class AdministradorasComponent implements OnInit {

  administradoras: Administradoras[]

  constructor( private administradorasService: AdministradorasService) { }

  ngOnInit(): void {
    this.administradorasService.getAdministradoras()
      .subscribe(administradoras => this.administradoras = administradoras);
  }

}
