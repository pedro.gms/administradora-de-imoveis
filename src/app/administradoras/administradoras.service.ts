import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Administradoras } from './administradoras.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ADM_API } from '../app.api';

@Injectable({
  providedIn: 'root'
})
export class AdministradorasService {

  constructor(private http: HttpClient) { }

  getAdministradoras(): Observable<Administradoras[]> {
    return this.http.get(`${ADM_API}/administradoras`)
      .pipe(map(response => response as Administradoras[]));
  }
}
