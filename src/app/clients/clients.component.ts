import { Component, OnInit } from '@angular/core'
import { ClientsService } from './clients.service'
import { Clients } from './clients.model'

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
})
  

export class ClientsComponent implements OnInit {

  clients: Clients[]

  constructor(private clientsService: ClientsService) { }

  ngOnInit(): void {
    
    this.clientsService.getClients()
      .subscribe(clients => this.clients = clients)
      
  }

}
