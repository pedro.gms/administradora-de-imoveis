import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs'

import { ADM_API } from '../app.api';
import { Clients } from './clients.model';

@Injectable({
  providedIn: 'root'
})
  
  
export class ClientsService {

  constructor(private http: HttpClient) { }

  getClients(): Observable<Clients[]> {
    return this.http.get(`${ADM_API}/clientes`)
      .pipe(map(response => response as Clients[]));
  }
  
}
