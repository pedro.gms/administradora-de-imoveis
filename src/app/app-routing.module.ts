import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministradorasComponent } from './administradoras/administradoras.component';
import { ClientsComponent } from './clients/clients.component';


const routes: Routes = [

  { path: 'clientes', component: ClientsComponent },
  { path: 'administradoras', component: AdministradorasComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
